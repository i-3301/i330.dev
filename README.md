# My Cool Website :3
I made this website from scratch because I hate templates and frameworks. There
is no JavaScript, it's all just html, css, and php (just as god intended). I
tried to make the site as modular and easy to update as possible, but like all
things it's definitely not perfect. If you want to use my design for your own
use, feel free to copy it. Just please make some changes to it to really make it
your own :))

The site does require php-fpm in order to run, so make sure you have that
installed and integrated with your webserver of choice. I do have a few unused
snippits of code that you can check out if you're looking to set up dynamic
webrings, but honestly I think it's pretty useless. If you have any questions at
all, feel free to shoot me an email (It's on my profile).

I also have a repo for the russian mirror of this site, but it's updated
significantly less often. I recommend that you grab changes from the english
repo before checking the russian one. Enjoy :))
