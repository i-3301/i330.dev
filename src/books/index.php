<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : Books</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="books.css">

  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="IDK man :/">
  <meta name="keywords" content="books,epub,archive,distopian,cyberpunk">

  <meta property="og:title" content="I-330 : Books">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="Archive of my favorite books, all english epubs.">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2023-01-25">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>

  <!-- HEADER -->
  <?php
    $location = "/books/";
    $title = "Books";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="book">
		<div class="book_about">
			<p>
				Below is an archive of books that I (and others) think are worth a read.
				All books are in English, (mostly) epub formatted, and free to download. 
        I think it's important for everyone to read, and I believe that regular 
        reading can help to improve mental health and overall well-being. I 
        won't force you to read any of these, but they are here if you want em. 
        If you want more, you should check out my <a href="archive/">archive</a> 
        where I keep a bunch of boring technical books.
			</p>
		</div>	
	</div>
  
  <!-- TODO AUTOMATE BOOK DISPLAY WITH PHP -->
  <div class="book_content">
  <?php
    $myFile = "book_names.txt";
    $lines = file($myFile);
    foreach ($lines as $x) {
      echo("<div class=\"book_item\">");
      echo("<a href=\"archive/novels/" . $x . "\" target=\"_blank\">");
      echo("<img src=\"img/novels/" . substr($x, 0, strpos($x, ".")) . ".jpg\" height=\"250px\" width=\"160px\" alt=\"" . $x . "\">");
      echo("</a>");
      echo("</div>");
    }
  ?>
  </div>

  <!-- FOOTER -->
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>
  
</body>






















