<!-- Page Footer -->

<footer>
	<div class="footer_border">
	   <a></a>
	</div>
	
	<!-- Webrings: I used static links, but these could easily be dynamically generated with php -->
	<div class="webRing">
	  <a href="https://azuremillennium.neocities.org" style="text-decoration:none;">↞</a>
	  <a href="https://teethinvitro.neocities.org/webring/linuxring/" style="text-decoration:none;"><strong>*nixRing</strong></a>
	  <a href="https://alan460.neocities.org" style="text-decoration:none;">↠</a>
	</div>
	
	<div class="webRing">
	  <a href="https://myyolo1999.blogspot.com/p/ja.html" style="text-decoration:none;">↞</a>
	  <a href="https://voicedrew.xyz/wr/tag.html" style="text-decoration:none;"><strong>Agora WebRing</strong></a>
	  <a href="https://idelides.neocities.org" style="text-decoration:none;">↠</a>
  </div>
 
 	
  <!-- Badges: All of these are inline so that way the spaces between images don't
    become links. There's probably a better way to do this, but I don't know
    what that would be, and this was a pretty easy fix :)) -->
	<div class="footer_badges" style="padding-bottom: -30px;">
	  <a href="/button/" target="_blank"><img src="/img/badges/i330.gif" alt="I-330 |"></a>
	  <a href="https://forum.agoraroad.com" target="_blank"><img src="/img/badges/agora.gif" alt="agora road |"></a>
	  <a href="https://store.steampowered.com/agecheck/app/1490060" target="_blank"><img src="/img/badges/disillusion.gif" alt="disillusion |"></a>
	  <a href="https://thoughtcrimes.neocities.org" target="_blank"><img src="/img/badges/thoughtcrimes.png" alt="thoughtcrimes |"></a>
	  <a href="https://zalazalaza.xyz" target="_blank"><img src="/img/badges/zalaz.gif" alt="zalaz |"></a>
	  <a href="https://dorgon.neocities.org/" target="_blank"><img src="/img/badges/dorgongif.gif" alt="dorgon"></a>
	  <a href="https://myyolo1999.blogspot.com/" target="_blank"><img src="/img/badges/some_porcupine.jpg" alt="Some Porcupine"></a>
	  <a href="https://idelides.neocities.org/" target="_blank"><img src="/img/badges/idelides.png" alt="Idelides"></a>
	  <a href="https://h00.neocities.org" target="_blank"><img src="/img/badges/h00.png" alt="Idelides"></a>
    <a href="https://river.rip/" target="_blank"><img src="/img/badges/river.gif" alt="river"></a>
    <a href="https://blog.shr4pnel.com/" target="_blank"><img src="/img/badges/shr4pnel.gif" alt="shr4pnel"></a>
	</div>
</footer>
