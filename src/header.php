<!-- Header: pass $location and $title string variables when importing so that 
the ru link functions, and the title displays correctly -->

<a href="https://ru.i330.dev<?php echo $location?>"><img class="change-language" src="/img/misc/ru.jpg"></a>

<div class="header">

  <h1>
  I-330 - <?php echo $title; ?>
  </h1>

  <nav>
    <span>
      <a href="/">home</a>
    </span>
    <span>|</span>
    <span>
      <a href="/posts">posts</a>
    </span>
    <span>|</span>
    <span>
      <a href="/books">books</a>
    </span>
  </nav>

</div>
