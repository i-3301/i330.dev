<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Metadata & SEO -->
  <title>I-330 : Home</title>
  
  <meta charset="utf-8">
  
  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">
  
  <meta name="viewport" content="width=device-width"/>
  <meta name="description" content="IDK man :/">
  <meta name="keywords" content="linux,unix,cybersecurity,gentoo,hacking">

  <meta property="og:title" content="I-330 : Home">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="Blog, Archive, and government psyop.">

  <!-- Page Resources -->
  <link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/style/home.css">

  <link rel="icon" type="image/png" sizes="32x32" href="img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/fav/favicon-16x16.png">
</head>

<body>
  <?php 
    $location = "";
    $title = "Home";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php'; 
  ?>

  <div class="content">
    <div style="display: flex; flex-wrap: wrap; justify-content: center;">

      <!-- Profile Picture -->
      <div style="flex-grow: 1; flex-shrink: 1; flex-basis: 30%; min-width: 200px; max-width: 70%;">
        <img src="img/misc/pfp.png" align="left" alt="8-BIT Banksy pfp" style="margin-top: 20px;">
      </div>

      <!-- About Me -->
      <div class="about" style="flex-grow: 2; flex-shrink: 2; flex-basis: 60%; ">
        <h2>
        About:
        </h2>
        <p>
          Hi, I am I-330. I am a Linux enthusiast, photography enjoyer, and a fancier of cute guys <3
        </p>
        <p>
          I am a studying cybersecurity engineer, currently working in software 
          engineering. I spend a lot of time tinkering with 
          <a href="img/misc/itsaunixsystem.gif" target="_blank">unix systems</a>, 
          and server hardware/software. I also enjoy reading about how we're 
          all going to be killed by robots, government, or ourselves 
          <a href="books/">(feel free to read along with me)</a>.
          While I don't have a social media you can reach me at, you can always
					shoot me an <a href="mailto:contact@i330.dev" target="_blank">E-mail</a> 
          (<a href="/i330.asc" target="_blank">PGP</a>).
          And hey, while you're here make sure to sign your name on my 
					<a href="https://www.yourworldoftext.com/~I330/" target="_blank">
					guestbook</a>!
        </p>
      </div>
    </div>
    
    <!-- My Projects -->
    <div style="flex-grow: 2; flex-shrink: 2; flex-basis: 90%;">
      <h2>
        Projects:
      </h2>
      <div style="display: flex; flex-wrap: wrap; justify-content: center;">

        <table style="width: 100%; text-align: left; margin-left: 1vw; border-spacing:10px;">
          <tr>
            <td style="width: 10%;"><a href="https://gitlab.com/i-330/i330.dev" target="_blank">i330.dev</a></td>
            <td style="width: 10%;"><img id="optionalstuff" src="/img/tags/html.png"></td>
            <td style="width: 16%;"><i>January 2023 - Present</i></td>
            <td style="width: 35%;">Blog, Archive, and government psyop.</td>
          </tr>
          <tr>
            <td><a href="https://radio.mocrd.org" target="_blank">Web Radio</a></td>
            <td><img id="optionalstuff" src="/img/tags/radio.gif"></td>
            <td><i>October 2022 - Present</i></td>
            <td>Self-Hosted Internet Radio [Shouts to <a href="https://h00.neocities.org" target="_blank">@h00</a>]</td>
          </tr>
					<tr>
            <td><a href="https://gitlab.com/I-330/retrowebdesktop" target="_blank">Web Desktop</a></td>
            <td><img id="optionalstuff" src="/img/tags/html.png"></td>
            <td><i>February 2024 - Present</i></td>
            <td>Framework for building web desktops</td>
          </tr>

        </table>
      </div>
    </div>
  </div>

  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>
 
</body>
