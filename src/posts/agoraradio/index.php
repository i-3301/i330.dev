<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : Agora Radio</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/posts/style/post.css">
	
  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="How the Agora Radio works on a slightly technical level">
  <meta name="keywords" content="icecast2,ffmpeg,radio,rtmp,vlc,radio">

  <meta property="og:title" content="I-330 : Agora Radio">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="How and why the Agora radio works.">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2023-01-31">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>

  <!-- HEADER -->
  <?php
    $location = "/posts/agoraradio/";
    $title = "Posts";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="blog">
    <div class="blog_byline">
      Posted: January 28, 2023 by I-330
    </div>
    <div class="blog_title">
      <h2>Agora Radio. Why it "doesnt" suck</h2>
    </div>
    <!-- <div class="blog_img">
      <img src="radiopage.png" width="100%">
		</div> -->
    <div class="blog_content">
			<h3>Preface :</h3>
      <p style="margin-top: -10px;">
        Are you a <i>nerd</i> that listens to music all day? Do you want to 
        force your music tastes onto other people? Well than this is the post 
        for you. Over the past couple months, in collaboration with agora road's
        only <a href="https://h00.neocities.org/" target="_blank">@h00</a>, 
				we created a radio station with the sole purpose of 
        converting vaporwave fans into Drum & Bass appreciators.
			</p>
      <p>
        So how does it work? Well, like all good things in life, it begins with 
        an insecure rtmp stream running from a terrible internet connection 
        thousands of miles from its public relay. The stream itself has been 
        online since mid 2022, but has only been actively broadcasting since 
        late 2022. If you want to check it out, you can give it a listen 
        <a href="https://radio.mocrd.org" target="_blank">here</a>.
      </p>
      <h3>RTMP :</h3>
      <p style="margin-top: -10px;">
        The initial rtmp stream originates from an old Dell PC which renders the
        visualizer, plays the audio, and send the broadcast via OBS to a bounce
        server hidden somewhere in the Central United States. Once it hits the 
        bounce server, the stream is re-broadcast publicly with significantly 
        more bandwidth and security, where it can be listened in on by anyone 
        with an rtmp compatible media player (<a href="https://www.videolan.org/vlc/" target="_blank">vlc</a>, 
        <a href="https://mpv.io/" target="_blank">mpv</a>, <a href="https://kodi.tv/" target="_blank">kodi</a>). 
      </p>
      <p>
        However, because you can't tune-in to an rtmp stream through a web
        browser, some adjustments had to be made to the stream. Using ffmpeg, we 
        were able to convert the rtmp stream into two seperate audio & 
        audio/video streams. These streams are then passed through an 
        application called icecast2 which creates a web-based video & audio 
        stream which can be embeded into any webpage. <div id="optionalstuff">
        The ffmpeg script:</div>
      </p>
      <div id="optionalstuff" class="code_block" style="border-style: solid;">
        <code>
          while true;<br>
            do ffmpeg -i rtmp://162.142.45.168:420/agora/radio \<br>
              -acodec libopus -b:a 64k \<br>
              -vcodec libvpx -b:v 500K -crf 25 -vf scale=720:480 -content_type video/webm \<br>
              icecast://source:PASSWD@LOCALIP:PORT/livevid.webm;<br>
            sleep 10;<br>
          done<br>
        </code>
      </div>

      <h3>ICECAST2 & WEB :</h3>
      <p style="margin-top: -10px;">
        The Icecast2 stream isn't perfect though, and it must be encrypted in 
        order for modern web browsers to support it. To do this, I created a 
        cloudflare tunnel from my server and forwarded its traffic to a 
        pre-configured domain address. This provided both a way to ensure SSL 
        encryption, as well as providing a globally available control panel that 
        I can use to adjust icecast2 settings on the fly.
      </p>

      <p>
        From here, the rest was relatively easy. I designed a simple web interface
        for people to listen in on, and embeded the Icecast2 audio stream. The 
        WinAmp player on the site was imported from a project called 
        <a href="https://github.com/captbaritone/webamp" target="_blank">Webamp</a>, which 
        re-implemented Webamp2 in Javascript with live audio playback and Milkdrop
        support. I am still working on a way to embed the video stream without 
        destroying my bandwidth or killing the video quality. However, I do plan 
        on having an alternative to the rtmp video live on the site soon... <sub>maybe.</sub>
      </p>
		</div>
	</div>

  <!-- FOOTER -->
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>

</body>






















