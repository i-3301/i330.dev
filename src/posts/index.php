<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : Posts</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/posts/style/root.css">
	
  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="IDK man :/">
  <meta name="keywords" content="linux,unix,cybersecurity,gentoo,hacking">

  <meta property="og:title" content="I-330 : Posts">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="dive into the decaying mind of I-330">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2023-01-25">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>
  
  <!-- HEADER -->
  <?php
    $location = "/posts/";
    $title = "Posts";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="blog">
		<div class="blog_about">
			<p>
        Lots of <i>fantastic</i> posts coming soon on tons of stupid stuff. Not 
        sure what yet, but it'll probably be something <b>cool</b>... <sub>probably.
				<a href="/posts.rss"><img src="/img/misc/rss.png" style="width: 15px;"></a></sub>
			</p>
		</div>	
	</div>
  
  <div class="blog_content">
    <h3 style="margin-bottom: 10px;">Posts :</h3>

		<div style="padding-top: 5px;">
				<table>
      		<tr>
						<td style="width: 20em;">
							<a href="lemon-pie" style="padding-left: 5px; text-decoration: none;">
							• How to make a yummy lemon pie :3
							</a>
						</td>
						<td style="display: inline;">
							<a href="lemon-pie" style="padding-left: 5px; text-decoration: none;" class="date_hide">
							|| 
							<i>
							2024-01-01
							</i>
							||
							</a>
						</div>
					</td>
					<tr>
						<td style="width: 20em;">
							<a href="no_js" style="padding-left: 5px; text-decoration: none;">
							• Replacing EVIL JavaScript with PHP
							</a>
						</td>
						<td style="display: inline;">
							<a href="no_js" style="padding-left: 5px; text-decoration: none;" class="date_hide">
							|| 
							<i>
							2023-06-20
							</i>
							||
							</a>
						</div>
					</td>
      		<tr>
						<td style="width: 20em;">
							<a href="widevine-001" style="padding-left: 5px; text-decoration: none;">
							• Breaking Widevine [Ep. 01]
							</a>
						</td>
						<td style="display: inline;">
							<a href="widevine-001" style="padding-left: 5px; text-decoration: none;" class="date_hide">
							|| 
							<i>
							2023-04-05
							</i>
							||
							</a>
						</td>
					</tr>
      		<tr>
						<td style="width: 20em;">
							<a href="agoraradio" style="padding-left: 5px; text-decoration: none;">
							• Agora Radio. Why it "doesn't" suck 
							</a>
						</td>
						<td style="display: inline;">
							<a href="agoraradio" style="padding-left: 5px; text-decoration: none;" class="date_hide">
							||
							<i>
							2023-01-31
							</i>
							||
							</a>
						</td>
					</tr>
    		</table>
			</a>
		</div>
  </div>


  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>

</body>

