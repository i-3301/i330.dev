<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : Lemon Pie</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/posts/style/post.css">

  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="How to make a yummy lemon meringue pie">
  <meta name="keywords" content="baking, pie, lemon meringue, food">

  <meta property="og:title" content="I-330 : How to make a yummy lemon meringue
  pie">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="Here's a chill and relaxing recipe
  for lemon meringue pie">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2024-01-01">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>

  <!-- HEADER -->
  <?php
    $location = "/posts/lemon-pie/";
    $title = "Posts";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="blog">
    <div class="blog_byline">
      Posted: January 01, 2024 by I-330
    </div>
    <div class="blog_title">
      <h2>A yummy lemon meringue pie recipe :3</h2>
    </div>
    <div class="blog_content">
			<h3>PIE! :</h3>
      <p style="margin-top: -10px;">
        Happy new year! I haven't posted in a while, and while I have been working on cool
        technical projects, none of them are finished enough for me to write
        anything interesting about them. Because of this, I figured I'd share an easy and
        delicious lemon pie recipe instead :)) It's very yummy and can be made
        easily with basic store-bought ingredients and little baking knowledge.
        The ingredients and instructions do not need to be followed to a T,
        and I have found that trusting your own instincts will lead to the best
        results :3
      </p>
      <h3>Ingredients :</h3>
      <p style="margin-top: -10px;">
        - 3 Eggs <br>
        - 1 Can of sweetened condensed milk (14oz) [<i>i personally recommend La
          Lechera if you can find it</i>]<br>
        - 1 Box of graham crackers <br>
        - 1-2 bars of salted butter (depending on size and consistency of crust)
        <br>
        - 1/2 cup of fresh squeezed lemon juice <br>
        - 1 tbsp of sugar
      </p>
      <h3>Instructions :</h3>
      <p style="margin-top: -10px;">
      &gt; First pre-heat your oven to 400°f (200°c) </p>
      <p>&gt; Next, to create the crust for your pie, crush graham crackers and butter
        together into a ~9in pie pan until you have a solid but still moist dough (This
        is easier if the butter is just a little soft).
        Spread this around the bottom and edges of your pie pan until you are
        happy with the crust. Typically I like a thin pie crust, but this tastes
        good thin or thick ;)) </p>
      <p>&gt; To create the pie batter, first separate the yolk and whites from three
        eggs, putting the yolks and whites in separate mixing bowls. </p>
      <p>&gt; In the bowl with the egg yolks, mix one can of condensed milk as well as
        1/2 cup of lemon juice. Mix gently until evenly distributed. </p>
      <p>&gt; In the egg white bowl, beat in a table spoon of sugar until you have a
        stiff meringue. You can add vanilla extract and a pinch of salt to the
        meringue if you want to add some extra flavor. (This process is much
        much quicker if you use a hand/stand mixer, though it is possible with
        just a whisk)</p>
      <p>&gt; Once both the filling and the meringue are completed, pour the filling
        into your pie mold and then gently spread your meringue on top. </p>
      <p>&gt; Finally, lower your oven temp to 350 and let your pie bake for around
        15 minutes or until the meringue is a good golden brown. </p>
      <p>&gt; Remove the pie from the oven and then let cool for 10-20 minutes before
      refrigerating
      </p>
      <h3>Finale :</h3>
      <p style="margin-top: -10px;">
        I know this isn't my usual type of post, but i figured I should write up
        something, and this is one of my favorite recipes to make. If you have
        any questions at all, or if you would like to recommend an adjustment,
        feel free to <a href="mailto:contact@i330.dev" target="_blank">reach out to me</a> :33 Happy baking!
      </p>
	  </div>
  </div>

  <!-- FOOTER -->
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>

</body>






















