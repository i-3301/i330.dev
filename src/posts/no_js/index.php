<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : No JS</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/posts/style/post.css">
	
  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="I hate js, so I re-wrote the webring in php">
  <meta name="keywords" content="javascript,php,webring,server">

  <meta property="og:title" content="I-330 : No JS">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="I hate PHP slightly less than JS">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2023-06-20">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>

  <!-- HEADER -->
  <?php
    $location = "/posts/no_js/";
    $title = "Posts";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="blog">
    <div class="blog_byline">
      Posted: June 20, 2023 by I-330
    </div>
    <div class="blog_title">
      <h2>Re-writing the Agora Webring in php</h2>
    </div>
    <div class="blog_content">
			<h3>What? :</h3>
      <p style="margin-top: -10px;">
			  Recently I was invited to join the Agora Road webring hosted by agora
        road user <a href="https://voicedrew.xyz" target="_blank">voicedrew</a>. 
        This awesome webring was built with the 
        <a href="https://garlic.garden/onionring/" target="_blank">onionring</a>
        framework, which while super neat is entirely written with javascript.
        Due to this, if I were to embed the webring widget on my page, I would
        be forced to introduce disgusting javascript to my pure and unspoiled
        website. I was not going to let this happen.
      </p>
      <p>
        In truth, I actually have nothing against javascript on websites. I 
        think it provides a lot of helpful tools to web developers and it can be
        used safely on websites both large and small. That said, because I 
        receive a somewhat large percentage of traffic via tor & terminal
        based web browsers such as <a href="https://en.wikipedia.org/wiki/Lynx_(web_browser)" target="_blank">lynx</a>,
        I personally wish to avoid using javascript on my page wherever possible.
      </p>
      <h3>PHP comes to the rescue :</h3>
      <p style="margin-top: -10px;">
        I had toyed around with a few different back-end tools from jhidra to node,
        but none of them really worked as well as I had hoped. I wanted something 
        that I could tinker with easily and wouldn't use much resources on my server.
        Eventually I decided on just using PHP, which thankfully integrated perfectly
        into my nginx server. With a bit of help to learn the basics of php (shouts to
        <a href="https://judydev.net/" target="_blank">judydev</a>), I had a 
        functional prototype for a php-based webring widget. All I had to do was
        create a page for each logic-defined button and I linked them on my front-facing
        widget.
      </p>
      <p>
        Originally I considered integrating the php directly into the embed, however
        I later decided that it would be easier to manage across my site if I just
        created a php file for each link instead. That being said, I don't see there
        being an issue if someone were to try integrating the php directly into the
        widget itself.
      </p>
      <h3>The code :</h3>
      <p style="margin-top: -10px;">
        As for the code itself, I really phoned it in. The actual php command is 
        just a bunch of unix commands piped into eachother. While I'm sure that 
        there isn't anything practically wrong with doing it this way, it does
        feel a bit cheap. Maybe in the future I'll come back to it and use php 
        commands to do all of this, but in the meantime I think this solution
        does the job. Feel free to check out the code <a href="https://gitlab.com/I-330/i330.dev/-/tree/main/php" target="_blank">here</a>.
      </p>
      <div id="optionalstuff" class="code_block" style="border-style: solid;">
        <code>
          <&#63;php
            <br>// curls the list of users in the webring, removes the current
            <br>// page, and then redirects to a random one of those listed users.
            <br><br>exec("curl 'https://voicedrew.xyz/wr/onionring-variables.js' \
            | sed -n \"/^'/s/^'//;s/',$//p\" | grep -v 'https://i330.dev' \
            a| shuf | head -n 1", $out);
            <br>header("Location: ". $out[0]);
          <br>&#63;>
        </code>
      </div>
      <br>
      <div id="optionalstuff" class="code_block" style="border-style: solid;">
        <code>
        <&#63;php
          <br>// curls the list of users, and then redirects to the user after
          <br>// the user mentioned in the grep command. (this may not work if
          <br>// you are the last user in the list btw)
          <br><br>exec("curl 'https://voicedrew.xyz/wr/onionring-variables.js' | sed -n \"/^'/s/^'//;s/',$//p\" | grep -i 'https://i330.dev' -A 1 | tail -n 1", $out);
          <br>header("Location: ". $out[0]);
        <br>&#63;>
        </code>
      </div>
      <p>
        This was a fun project to work on and I'm very happy that I was invited 
        to the agora webring. I hope that this can help anyone else who is using
        onionring but doesn't want to bother with javascript (niche market ik). 
        If you have any questions about how I configured my server or about 
        literally anything else whatsoever, feel free to email me! My email is 
        contact@i330.dev
        <br>
        Have a great day!
      </p>
		</div>
	</div>

  <!-- FOOTER -->
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>  

</body>

