<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>I-330 : Breaking Widevine [Ep. 01]</title>
  
	<link rel="stylesheet" href="/style/style.css">
	<link rel="stylesheet" href="/posts/style/post.css">

  <meta http-equiv="onion-location" content="http://i33devc5jzkmmxxhtnkk37vljci44e2p6dkb6fgt5w6gwzjljcojicyd.onion">

	<meta name="viewport" content="width=device-width"/>
  <meta name="description" content="Join me on a journey to break every bone in widevines body.">
  <meta name="keywords" content="widevine,drm,hacking,analysis,opensource">

  <meta property="og:title" content="I-330 : Breaking Widevine [Ep. 01]">
  <meta property="og:url" content="https://i330.dev">
  <meta property="og:site_name" content="I-330">
  <meta property="og:description" content="Watch me cripple poor helpless widevine!">

  <meta name="articly:author" content="I-330">
  <meta name="article:published_time" contents="2023-04-05">

  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/fav/favicon-32x32.png">
</head> 

<body>

  <!-- HEADER -->
  <?php
    $title = "Posts";
    $location = "/posts/widevine-001/";
    require_once $_SERVER['DOCUMENT_ROOT'].'/header.php';
  ?>

  <div class="blog">
    <div class="blog_byline">
      Posted: April 05, 2023 by I-330
    </div>
    <div class="blog_title">
      <h2>Breaking Widevine [Ep. 01]</h2>
    </div>
    <!-- <div class="blog_img">
      <img src="radiopage.png" width="100%">
		</div> -->
    <div class="blog_content">
			<h3>Preface :</h3>
      <p style="margin-top: -10px;">
        Have you ever watched a Netflix video and thought to yourself "I really
        wish I could just download this?" Well I would never because I despise 
        the idea of violating intellectual property laws ;3. However, if someone
        were to have that goal in mind, the first thing they'd have to bypass is
        Googles infamous DRM software widevine.
      </p>
      <p>
        In collaboration with amazing computer engineer
        <a href="https://twitter.com/BluNaxela" target="_blank">Alex Wilson</a>,
        we've been able to break apart much of widevine's features and exploit 
        its implementation inside of the most superior web browser... Firefox! Firefox
        not only has the most open platform for poking and prodding, it also gives
        easy access to all of the tools it uses to interact with the widevine 
        libraries. Firefox also has a wonderful 3rd party tool called 
        <a href="https://searchfox.org/" target="_blank">serachfox</a> which makes 
        searching through source code super simple!
      </p>
      <h3>Analyzing Widevine :</h3>
      <p style="margin-top: -10px;">
        Before starting with the brutalities, the first step was to figure out how 
        firefox implements widevine in the first place. Knowing this will help later when 
        trying to exploit it. Based on the limited analysis we've done on 
        firefox's widevine implementation, it seems that once the video is 
        decrypted, it is just as accessible as any regular video embed. This 
        means that in theory, all that's needed in order to retrieve the original
        video data is to intercept the frames as they are decoded by widevine.
      </p>
      <p>
        Now, while intercepting decrypted video frames does help to bypass a large
        part of the decoding process, it does have it's own draw backs. Firstly,
        you still need to worry about adaptive bit-rate, and secondly you will
        need to figure out a way to request more frames from the video server 
        than typical during regular video playback.
      </p>
      <h3>Breaking Widevine in Half:</h3>
      <p style="margin-top: -10px;">
        Of course an idea is only half of the battle, implementing it is another
        beast all together. Obviously mozilla, is going to put in some barriers 
        to prevent the user from simply capturing frame data right?... right?
        NOPE! With a couple lines of code you can bypass almost every 
        restriction mozilla has built to prevent the user from grabbing frames.
      </p>
	  <p>
		The first hurdle to jump is firefox's restriction on recording DRM 
		restricted video. While you might think this would be difficult to do,
        it's really just as simple as un-commenting a single if statement. 
        Seriously, the only think keeping you from sharing your netflix stream
        with friends on discord is a single if statement. 
      </p>
      <div id="optionalstuff" class="code_block" style="border-style: solid;">
        <code>
          // /dom/html/htmlmediaelement.cpp#3796<br>
          // prevent capturing restricted video<br>
		      if (acapturetype == streamcapturetype::capture_all_tracks && 
			    containsrestrictedcontent()) {<br>
		        &nbsp;&nbsp;return false;<br>
		      }<br>
		      return true;<br>
        </code>
      </div>

      <p>
        While that's cool and all, you're still going to be recording heavily 
        encoded and video with heavy artifacting. If only there were an easy way 
        to capture the frames immediately after they're decoded. If only firefox
        had a built in method specifically for capturing the frames of DRM
        protected content... Oh wait, they do! In 
        <a href="https://searchfox.org/mozilla-central/source/dom/media/gmp/ChromiumCDMChild.cpp" target="_blank">dom/media/gmp/ChromiumCDMChild.cpp</a>,
        they give you all the tools you need to grab frame info, codecs, and 
        save every frame decoded by Widevine. 
      </p>

      <p>
        Below is a snip of code which can capture, write, and format captured 
        video data directly from widevine and append it to a file as raw yuv420
        video. While I'm aware that this isn't a perfect appproach, it's a big 
        leap towards high res widevine video capture as it doesn't distinguish 
        between L1, L2, or L3 encryption.
      </p>
      <div id="optionalstuff" class="code_block" style="border-style: solid;">
        <code>
          WidevineVideoFrame frame;<br>
          cdm::Status rv = mCDM->DecryptAndDecodeFrame(input, &frame);<br>
          <br>
          //====[Widevine Frame Extraction]=====|<br>
          // Make sure to build/run with file sandboxing disabled<br>
          <br>
          printf("Decrypted frame! w:%i h:%i f:%i\n", frame.Size().width, frame.Size().height, frame.Format());<br>
          printf("\tOffsets - Y:%i U:%i V:%i\n", frame.PlaneOffset(cdm::kYPlane), frame.PlaneOffset(cdm::kUPlane), frame.PlaneOffset(cdm::kVPlane));<br>
          printf("\tStride - Y:%i U:%i V:%i\n", frame.Stride(cdm::kYPlane), frame.Stride(cdm::kUPlane), frame.Stride(cdm::kVPlane));<br>
          <br>
          if (frame.Size().width > 0) {<br>
            &nbsp;&nbsp;FILE *fptr;<br>
            &nbsp;&nbsp;fptr = fopen("/tmp/widevine_frame.yuv", "ab");<br>
            &nbsp;&nbsp;fwrite(frame.FrameBuffer()->Data(), frame.FrameBuffer()->Size(), 1, fptr);<br>
            &nbsp;&nbsp;fclose(fptr);<br>
          }<br>
          //====[End of Extraction]=============|
        </code>
      </div>
      <p>
        While it's not a perfect implementation by any means, there seems to be
        many ways to further break firefox's implementation of widevine, next 
        on the list being audio extraction and rapid frame requesting. If those
        two hurdles can be cleared, we'll be one step closer to the dream of 
        right-click downloading DRM protected content.
      </p>
	  </div>
  </div>

  <!-- FOOTER -->
  <?php require_once $_SERVER['DOCUMENT_ROOT'].'/footer.php'; ?>

</body>

